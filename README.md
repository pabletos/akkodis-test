# Akkodis Inditex technical test
This project is based on the provided pdf attached by the Akkodis recruitment team. Project is developed as is, including the insertion of some tests, as a proof of test writing.

## Bootstraping

Project was created using the template for typescript by create-react-app, as the fastest way of creating the initial project.

## Additional dependencies

A minimal CSS framework is added, called [Pico-CSS](https://picocss.com/), only for having a good CSS reset and defaults.

## Available scripts
### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
