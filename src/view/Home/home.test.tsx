import React from 'react';
import { render, fireEvent, act } from '@testing-library/react';
import { MemoryRouter, Route } from 'react-router-dom';
import { getPodcasts } from '../../script/api';
import { Podcast } from '../../interfaces/Podcast';
import PodcastCard from '../../component/PodcastCard';
import { AppContext } from '../../App';
import HomeView from '.';

jest.mock('../../script/api', () => ({
  getPodcasts: jest.fn(() => Promise.resolve([])),
}));

describe('HomeView', () => {
  const mockPodcasts: Podcast[] = [
	{
	  id: '1',
	  title: 'Podcast 1',
	  image: 'image-url-1',
	  description: 'Description 1',
	  author: 'Author 1',
	},
	{
	  id: '2',
	  title: 'Podcast 2',
	  image: 'image-url-2',
	  description: 'Description 2',
	  author: 'Author 2',
	},
  ];

  const mockContextValue = {
	podcasts: mockPodcasts,
	loading: false,
	setLoading: jest.fn()
  };

  beforeEach(() => {
	jest.clearAllMocks();
  });

  it('renders the home view correctly', async () => {
	// @ts-ignore
	getPodcasts.mockResolvedValueOnce(mockPodcasts);
	const { findByText, getByPlaceholderText } = render(
		<MemoryRouter>
			<AppContext.Provider value={mockContextValue}>
			<HomeView />
			</AppContext.Provider>
		</MemoryRouter>
	);

	expect(await findByText('2')).toBeInTheDocument();
	expect(getByPlaceholderText('Filter podcasts...')).toBeInTheDocument();

	const podcastCards = (await findByText('Podcast 1')).closest('.podcast-card');
	expect(podcastCards).toBeInTheDocument();
	expect(await findByText('Podcast 1')).toBeInTheDocument();
	expect(await findByText('Author 1')).toBeInTheDocument();
  });

  it('filters the podcast list based on search input', async () => {
	// @ts-ignore
	getPodcasts.mockResolvedValueOnce(mockPodcasts);

	  const { getByPlaceholderText, findByText, queryByText } = render(
		<MemoryRouter>
		  <AppContext.Provider value={mockContextValue}>
			<HomeView />
		  </AppContext.Provider>
		</MemoryRouter>
	  );

	  const searchInput = getByPlaceholderText('Filter podcasts...');
	  fireEvent.input(searchInput, { target: { value: 'Podcast 2' } });

	  expect(await findByText('1')).toBeInTheDocument();
	  expect(queryByText('Podcast 1')).not.toBeInTheDocument();
	  expect(await findByText('Podcast 2')).toBeInTheDocument();
	  expect(await findByText('Author 2')).toBeInTheDocument();

	  fireEvent.input(searchInput, { target: { value: '' } });

	  expect(await findByText('2')).toBeInTheDocument();
	  expect(await findByText('Podcast 1')).toBeInTheDocument();
	  expect(await findByText('Podcast 2')).toBeInTheDocument();
  });
});