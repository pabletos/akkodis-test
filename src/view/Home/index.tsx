import React, { useContext, useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";
import './style.css';
import { getPodcasts } from '../../script/api';
import { Podcast } from '../../interfaces/Podcast';
import PodcastCard from '../../component/PodcastCard';
import { AppContext } from '../../App';

export default function HomeView() {
	const [filteredPodcastList, setFilteredPodcastList] = useState<Podcast[]>([]);
	const navigate = useNavigate();
	const {podcasts, loading} = useContext(AppContext);
	useEffect(() => {
		getPodcasts().then((podcasts) => {
			setFilteredPodcastList(podcasts);
		}).catch((error) => {
		   console.error(error); 
		});
	}, [podcasts]);

	const createPodcastList = (podcasts: Podcast[]) => {
		return podcasts.map((podcast, idx) => {
			return (
				<PodcastCard
					onClick={() => navigate(`/podcast/${podcast.id}`)}
					key={idx}
					title={podcast.title}
					imgUrl={podcast.image}
					author={podcast.author}
				/>
			);
		});
	};

	const searchPodcast = (event: React.ChangeEvent<HTMLInputElement>) => {
		const searchValue = event.target.value;
		const filteredPodcastList = podcasts.filter((podcast) => {
			return podcast.title.toLowerCase().includes(searchValue.toLowerCase()) || podcast.author.toLowerCase().includes(searchValue.toLowerCase());
		});
		setFilteredPodcastList(filteredPodcastList);
	};

	return (
		<div className="home">
			{ !loading && 
				<header className="home-header">
					<span>{filteredPodcastList.length}</span>
					<input type="text" onInput={searchPodcast} placeholder="Filter podcasts..."/>
				</header>
			}
			<div className="podcast-list">
				{createPodcastList(filteredPodcastList)}
			</div>
		</div>
	);
}