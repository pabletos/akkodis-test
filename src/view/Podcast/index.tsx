import React, { useEffect, useState, useContext } from 'react';
import { getPodcastEpisodes } from '../../script/api';
import { Episode } from '../../interfaces/Episode';
import { Podcast } from '../../interfaces/Podcast';
import { useNavigate, useParams } from 'react-router-dom';
import './style.css';
import { formatMillis, formatDate } from '../../script/util';
import { AppContext } from '../../App';
import PodcastDetail from '../../component/PodcastDetail';

export default function PodcastView() {
	const [episodes, setEpisodes] = useState<Episode[]>([]);
	const [selectedPodcast, setSelectedPodcast] = useState<Podcast | null>(null);
	const navigate = useNavigate();
	const { id } = useParams();
	const {podcasts, loading, setLoading} = useContext(AppContext);
	useEffect(() => {
		if (id === undefined) {
			navigate('/');
		} else {
			setLoading(true);
			getPodcastEpisodes(id).then((episodes) => {
				setEpisodes(episodes);
				setLoading(false);
			}).catch(() => {
				navigate('/');
			});
		}
	}, [id]);
	useEffect(() => {
		setSelectedPodcast(podcasts.find((podcast) => podcast.id === id) || null);
	}, [podcasts]);
	// The big usage of article instead of div is just because that's how pico-css defines a card
	return (
		<div className="podcast grid">
			{ selectedPodcast === null ?
				<></> : 
				<PodcastDetail 
					title={selectedPodcast.title}
					image={selectedPodcast.image}
					author={selectedPodcast.author}
					description={selectedPodcast.description}
					onClick={() => navigate(`/podcast/${selectedPodcast.id}`)}
				/>
			}
			<div className="episodes">
				<article>
					<h2>{loading ? 'Loading episodes...' : `Episodes: ${episodes.length}`}</h2>
				</article>
				<article>
					<table>
						<thead>
							<tr>
								<th>Title</th>
								<th>Date</th>
								<th>Duration</th>
							</tr>
						</thead>
						<tbody>
						{
							episodes.map((episode, idx) => {
								return (
									<tr key={idx}>
										<td><a onClick={() => navigate(`/podcast/${selectedPodcast?.id}/episode/${episode.id}`)}>{episode.title}</a></td>
										<td>{formatDate(episode.date)}</td>
										<td>{formatMillis(episode.duration)}</td>
									</tr>
								);
							})
						}
						</tbody>
					</table>
				</article>
			</div>
		</div>
	)
}