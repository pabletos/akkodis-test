import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { MemoryRouter, Route, Routes } from 'react-router-dom';
import { AppContext } from '../../App';
import { Podcast } from '../../interfaces/Podcast';
import { Episode } from '../../interfaces/Episode';
import { getPodcastEpisodes } from '../../script/api';
import EpisodeView from '.';

jest.mock('../../script/api', () => ({
	getPodcastEpisodes: jest.fn().mockResolvedValue([]),
}));

describe('EpisodeView', () => {

const mockPodcast: Podcast = {
	id: '1',
	title: 'Test Podcast',
	image: 'test-image-url',
	author: 'Test Author',
	description: 'Test description',
};

const mockEpisode: Episode = {
	id: 1,
	title: 'Test Episode',
	description: 'Test episode description',
	media: 'test-media-url',
	author: 'Test Author',
	date: new Date(),
	duration: 11000
};

const mockContextValue = {
	podcasts: [mockPodcast],
	loading: false,
	setLoading: jest.fn(),
};

beforeEach(() => {
	jest.clearAllMocks();
});

test('renders the episode view correctly', async () => {
	// @ts-ignore
	getPodcastEpisodes.mockResolvedValue([mockEpisode]);
	const { findByText, findByAltText } = render(
		<AppContext.Provider value={mockContextValue}>
			<MemoryRouter initialEntries={['/podcast/1/episode/1']}>
				<Routes>
					<Route path="/podcast/:podcastId/episode/:episodeId" element={<EpisodeView />}/>
				</Routes>
			</MemoryRouter>
		</AppContext.Provider>
	);
	expect(await findByText('Test Podcast')).toBeInTheDocument();
	expect(await findByText('Test Episode')).toBeInTheDocument();
	const description = await findByText('Test episode description');
	expect(description).toBeInTheDocument();
	expect(await findByAltText('Test Podcast')).toBeInTheDocument();

	if (description !== null) {
		const audioElement = description.parentElement?.nextSibling as HTMLAudioElement;
		expect(audioElement).toBeInTheDocument();
	}
});

});