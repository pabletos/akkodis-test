import React, { useContext, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router';
import { AppContext } from '../../App';
import { Podcast } from '../../interfaces/Podcast';
import { Episode } from '../../interfaces/Episode';
import { getPodcastEpisodes } from '../../script/api';
import PodcastDetail from '../../component/PodcastDetail';
import './style.css';

export default function EpisodeView() {
	const {episodeId, podcastId} = useParams();
	const {podcasts, setLoading} = useContext(AppContext);
	const navigate = useNavigate();
	const [selectedPodcast, setSelectedPodcast] = useState<Podcast | null>(null);
	const [episode, setEpisode] = useState<Episode | null>(null);
	useEffect(() => {
		if (episodeId === undefined || podcastId === undefined) {
			navigate('/');
		} else {
			setLoading(true);
			getPodcastEpisodes(podcastId).then((episodes) => {
				const id = parseInt(episodeId);
				console.log(typeof episodeId, typeof episodes[0].id);
				setEpisode(episodes.find((episode) => episode.id === id) || null);
				setLoading(false);
			}).catch(() => {
				navigate('/');
			});
		}
	}, [episodeId, navigate, podcastId, setLoading]);
	useEffect(() => {
		setSelectedPodcast(podcasts.find((podcast) => podcast.id === podcastId) || null);
	}, [podcasts, podcastId]);
	return (
		<div className="episode grid">
			{ selectedPodcast === null ?
				<></> : 
				<PodcastDetail 
					title={selectedPodcast.title}
					image={selectedPodcast.image}
					author={selectedPodcast.author}
					description={selectedPodcast.description}
					onClick={() => navigate(`/podcast/${selectedPodcast.id}`)}
				/>
			}
			<article className="episode-detail">
				<hgroup>
					<h3>{episode?.title}</h3>
					<p>{episode?.description}</p>
				</hgroup>
				<audio src={episode?.media} controls></audio>
			</article>
		</div>
	)
}