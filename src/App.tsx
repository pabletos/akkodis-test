import React, {createContext, useState, useEffect} from 'react';
import { Podcast } from './interfaces/Podcast';
import { getPodcasts } from './script/api';
import {
	RouterProvider
} from "react-router-dom";
import Router from './script/router';

export interface Context {
	podcasts: Podcast[];
	loading: boolean;
	setLoading: (loading: boolean) => void;
}

export const AppContext = createContext<Context>({podcasts: [], loading: false, setLoading: () => {}});

function App() {
	const [context, setContext] = useState<Context>({podcasts: [], loading: false, setLoading: () => {}});
	useEffect(() => {
		setContext(curr => ({
			...curr,
			loading: true,
			setLoading: (loading: boolean) => {setContext(curr => ({...curr, loading}))}
		}));
		getPodcasts().then((podcasts) => {
			setContext(curr => ({...curr, podcasts, loading: false}));
		}).catch((error) => {
			console.error(error); 
		});
	}, []);
	return (
	<AppContext.Provider value={context}>
		<header>
			<a href='/'>Podcaster</a>
			{
				context.loading && <span className="lds-hourglass"/>
			}
		</header>
		<hr></hr>
		<main>
			<RouterProvider router={Router} />
		</main>
	</AppContext.Provider>
	);
}

export default App;
