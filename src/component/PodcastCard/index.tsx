import React from "react";
import "./style.css";

export interface PodcastCardProps {
	onClick: (event: React.MouseEvent<HTMLElement>) => void;
	title: string;
	imgUrl: string;
	author: string;
}

export default function PodcastCard(props: PodcastCardProps) {
	return (
		<article onClick={props.onClick} className="podcast-card">
			<img alt={props.title} src={props.imgUrl}/>
			<hgroup>
				<h4>{props.title}</h4>
				<div className="author">{props.author}</div>
			</hgroup>
		</article>
	);
}