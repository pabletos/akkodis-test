import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import PodcastCard, { PodcastCardProps } from '.';

describe('PodcastCard', () => {
  const defaultProps: PodcastCardProps = {
	onClick: jest.fn(),
	title: 'Test Podcast',
	imgUrl: 'test-image-url',
	author: 'Test Author',
  };

	it('renders the podcast card correctly', () => {
		const { getByText, getByAltText } = render(<PodcastCard {...defaultProps} />);

		const titleElement = getByText('Test Podcast');
		const authorElement = getByText('Test Author');
		const imageElement = getByAltText('Test Podcast');

		expect(titleElement).toBeInTheDocument();
		expect(authorElement).toBeInTheDocument();
		expect(imageElement).toBeInTheDocument();
	});

	it('calls the onClick event handler when clicked', () => {
		const { container } = render(<PodcastCard {...defaultProps} />);
		const articleElement = container.querySelector('article');

		expect(articleElement).toBeInTheDocument();
		if (!articleElement) {
			throw new Error('card is null');
		}
		fireEvent.click(articleElement);

		expect(defaultProps.onClick).toHaveBeenCalled();
	});
});