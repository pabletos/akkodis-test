import React from "react";
import "./style.css";

export interface PodcastDetailProps {
	onClick: (event: React.MouseEvent<HTMLElement>) => void;
	title: string;
	image: string;
	author: string;
	description: string;
}

export default function PodcastDetail(props: PodcastDetailProps) {
	return (
		<article className="details">
			<header>
				<img src={props.image} alt={props.title}/>
			</header>
			<hgroup>
				<h3>{props.title}</h3>
				<h4>{props.author}</h4>
			</hgroup>
			<footer>
				{props.description}
			</footer>
		</article>
	)
}