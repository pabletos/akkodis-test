import React from 'react';
import { render } from '@testing-library/react';
import PodcastDetail, { PodcastDetailProps } from '.';

describe('PodcastDetail', () => {
  const defaultProps: PodcastDetailProps = {
    onClick: jest.fn(),
    title: 'Test Podcast',
    image: 'test-image-url',
    author: 'Test Author',
    description: 'Test description',
  };

  it('renders the podcast details correctly', () => {
    const { getByText, getByAltText } = render(<PodcastDetail {...defaultProps} />);

    const titleElement = getByText('Test Podcast');
    const authorElement = getByText('Test Author');
    const descriptionElement = getByText('Test description');
    const imageElement = getByAltText('Test Podcast');

    expect(titleElement).toBeInTheDocument();
    expect(authorElement).toBeInTheDocument();
    expect(descriptionElement).toBeInTheDocument();
    expect(imageElement).toBeInTheDocument();
  });
});