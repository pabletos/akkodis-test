export function storage(key: string, value: any = undefined) {
	if (value === undefined) return JSON.parse(localStorage.getItem(key) || 'null');
	if (value === null) return localStorage.removeItem(key);
	return localStorage.setItem(key, JSON.stringify(value));
}

export function formatMillis(millis: number): string {
	const date = new Date(millis);
	const hours = date.getUTCHours();
	const minutes = date.getUTCMinutes();
	const seconds = date.getUTCSeconds();
	return `${hours ? hours + ':' : ''}${minutes < 10 ? '0' + minutes : minutes}:${seconds < 10 ? '0' + seconds : seconds}`;
}

export function formatDate(date: Date): string {
	const day = date.getUTCDate();
	const month = date.getUTCMonth() + 1;
	const year = date.getUTCFullYear();
	return `${day}/${month}/${year}`;
}