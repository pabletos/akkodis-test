import { createEpisode, Episode } from '../interfaces/Episode';
import {createPodcast, Podcast} from '../interfaces/Podcast';
import {storage} from './util';

const CACHE_EXPIRATION = 1000 * 60 * 60 * 24; // 1 day

export async function api(url: string, data: object, cached=false, method: 'GET' | 'POST' = 'GET', cors=true) {
	if (method === 'GET') {
		url += '?' + Object.entries(data).map(([key, val]) => `${key}=${val}`).join('&');
	}
	const encodedUrl = encodeURIComponent(url);
	if (cached) {
		const cachedData = storage(encodedUrl);
		if (cachedData && cachedData.expires > Date.now()) {
			return cachedData.data;
		}
	}
	let res = null;
	let isError = false;
	if (cors) {
		url = `https://api.allorigins.win/get?url=${encodedUrl}`;
	}
	try {
		res = await fetch(url, {
			headers: {'Content-Type': 'application/json'},
			method,
			credentials: 'include',
			body: method === 'POST' ? JSON.stringify({...data}) : undefined
		});
	} catch (e) {
		res = {msg: 'unexpectedError', params: []};
		throw res;
	}
	const {status} = res;
	if (res.headers.get('Content-Type') === 'application/json') {
		res = await res.json();
		if (cors) res = JSON.parse(res.contents);
	} else {
		res = await res.blob();
	}
	if (status >= 400) {
		isError = true;
		res = {msg: res, status};
	} else if (cached) {
		storage(encodedUrl, {data: res, expires: Date.now() + CACHE_EXPIRATION});
	}
	if (isError) throw res;
	return res;
}

export function getPodcasts(): Promise<Podcast[]> {
	return api('https://itunes.apple.com/us/rss/toppodcasts/limit=100/genre=1310/json', {}, true).then(res => {
		return res.feed.entry.map((podcast: any) => createPodcast(podcast));
	});
}

export function getPodcastEpisodes(id: string, limit: number = 100): Promise<Episode[]> {
	return api(`https://itunes.apple.com/lookup`, {id, media: 'podcast', entity: 'podcastEpisode', limit}, true).then(res => {
		if (res.resultCount === 0) throw new Error('Podcast not found');
		return res.results.slice(1).map((data: any) => createEpisode(data));
	});
}
