import {
	createBrowserRouter
} from "react-router-dom";
import Home from '../view/Home';
import Podcast from '../view/Podcast';
import Episode from "../view/Episode";

export default createBrowserRouter([
	{
		path: "/",
		element: <Home/>,
	},
	{
		path: "/podcast/:id",
		element: <Podcast/>
	},
	{
		path: "/podcast/:podcastId/episode/:episodeId",
		element: <Episode/>
	}
]);