export interface Episode {
	id: number;
	title: string;
	description: string;
	author: string;
    media: string;
    date: Date;
	duration: number;
}

export function createEpisode(data: any): Episode {
	return {
		id: data.trackId,
		title: data.trackName,
		description: data.description,
		author: data.collectionName,
        media: data.episodeUrl,
        date: new Date(data.releaseDate),
		duration: data.trackTimeMillis
	}
}