export interface PodcastDetails {
	id: string;
	title: string;
	image: string;
	description: string;
	author: string;
}

export function createPodcastDetails(data: any): PodcastDetails {
	return {
		id: data.collectionId,
		title: data.collectionName,
		image: data['im:image'][2].label,
		description: data.summary.label,
		author: data.artistName
	}
}