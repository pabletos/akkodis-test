export interface Podcast {
	id: string;
	title: string;
	image: string;
	description: string;
	author: string;
}

export function createPodcast(data: any): Podcast {
	return {
		id: data.id.attributes['im:id'],
		title: data['im:name'].label,
		image: data['im:image'][2].label,
		description: data.summary.label,
		author: data['im:artist'].label
	}
}